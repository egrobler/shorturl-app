# Short URL Generator App

This project contains 2 folders:
- backend: This folder contains the NodeJS / express application that will handle the API endpoints.
    - POST: /api/url {longUrl: ...long url to be shortened...}
    - GET: /s/...shortUrl
    - GET: /api/urls
- frontend: This is a small React UI that basically is a form where you can submit a long url to be shortened.

I am using Docker to create the following containers:
- backend (NodeJs + Express app for the API)
- frontend (React UI built from create-react-app)
- database (for mongodb to persist data. Used the docker mongo image, that's it.)

## To run the applicaiton:

1. Clone this repo to some folder locally

2. Run docker-compose build and docker-compose up to create and startup the 3 containers mentioned above.

```bash
docker-compose build
docker-compose up
```

3. The React App (frontend/UI) will now be running at http://localhost:4000
4. The mongodb database will be running at localhost:27017/shorty. Mongodb URI is mongodb://localhost:27017/shorty  (or mongodb://database:27017/shorty  from within the containers)

5. The API's can also be tested through a tool like Postman or Insomnia.

http://localhost:3000/api/url
    - POST
    - data: { longUrl: ... the long url ... }

http://localhost:3000/api/urls
    - GET
    - returns all the documents for the Links collection (like SELECT * FROM 'links' in SQL)
    - returns in JSON format

http://localhost:3000/s/...shorturl...
    - GET
    - This will match the give shorturl/hash against the documents in the database, and then redirect the user if it is there.