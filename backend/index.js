// Require express module
// require('dotenv').config();
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const PORT = process.env.API_PORT || 3000;

const mongoURI = process.env.MONGODB_URI || "mongodb://database:27017/shorty";

const connectOptions = {
  keepAlive: true,
  reconnectTries: Number.MAX_VALUE,
  useNewUrlParser: true
};

// Connecting to DB
mongoose.Promise = global.Promise;
mongoose.set("useFindAndModify", false);
mongoose.connect(
  mongoURI,
  connectOptions,
  (err, db) => {
    if (err) console.log(`Error`, er);
    console.log(`Connected to MongoDB`);
  }
);

const app = express();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-type,Accept,x-access-token,X-Key"
  );
  if (req.method == "OPTIONS") {
    res.status(200).end();
  } else {
    next();
  }
});
app.use(bodyParser.json());

require("./models/Links");
require("./routes/urlRoutes")(app);



// Starting web server

app.listen(PORT, () => {
    console.log(`Server http://localhost:${PORT}`);
});
