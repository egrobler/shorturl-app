//Create schema for the "Links" table.

const mongoose = require("mongoose");

const { Schema } = mongoose;
const linksSchema = new Schema({
  longUrl: String,
  urlHash: String,
  shortUrl: String,
  nrClicks: { type: Number, default: 0 },
  visited: {type: Boolean, default: false},
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

mongoose.model("Links", linksSchema);