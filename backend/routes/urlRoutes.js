const mongoose = require("mongoose");
const validUrl = require("valid-url");
const Links = mongoose.model("Links");
const shortid = require("shortid");
const PORT = process.env.API_PORT || 3000;
const shortBaseURL = `http://localhost:${PORT}/s/`;

module.exports = app => {
    app.get("/s/:code", async (req, res) => {
        const urlHash = req.params.code;
        const item = await Links.findOneAndUpdate({ "urlHash": urlHash }, { "visited": true, $inc: { "nrClicks": 1 } }, { new: true });
        
        if (item) {
            return res.redirect(item.longUrl);
        } else {
            return res.status(401).json("Couldn't find the shortURL");
        }
    });

    app.get("/api/urls", async (req, res) => {
        const items = await Links.find((err, links) => {
            // Note that this error doesn't mean nothing was found,
            // it means the database had an error while searching, hence the 401 status
            if (err) return res.status(401).send("Couldn't find the shortURL", err)
            // send the list of all links
            return links;
        }).sort({ createdAt: -1 });
        if (items) {
            return res.status(200).json(items);
        } else {
            return res.status(401).json("Couldn't find the shortURL");
        }
    });

    app.post("/api/url", async (req, res) => {
        const { longUrl } = req.body;
        const urlHash = shortid.generate();
        const updatedAt = new Date();

        if (validUrl.isUri(longUrl)) {
            try {
                const item = await Links.findOne({ longUrl: longUrl });
                if (item) {
                    res.status(200).json(item);
                } else {
                    shortUrl = `${shortBaseURL}${urlHash}`;
                    const item = new Links({
                        longUrl,
                        shortUrl,
                        urlHash,
                        updatedAt
                    });
                    await item.save();
                    res.status(200).json(item);
                }
            } catch (err) {
                res.status(401).json("Couldn't match the shortURL");
            }
        } else {
            return res.status(401).json("The Long URL isn't a valid URL");
        }
    });
};