import React, { Component } from "react";
import axios from "axios";

import Items from "./components/Items";
import { ShortUrlProvider } from "./ShortUrlContext";

class App extends Component {
  constructor(props) {
    super(props);
    // Setting initial state
    this.state = { value: "", items: [], shortUrl: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getUrlList = this.getUrlList.bind(this);
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  // The event that handles the url form input value
  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  // Handles the form submission.
  handleSubmit = async event => {
    if (event.currentTarget.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      event.preventDefault();
      event.currentTarget.classList.add("was-validated");

      // posts the longUrl value to the API to get the short url
      const { data: res } = await axios.post(
        `http://localhost:${process.env.REACT_APP_AXIOS_PORT}/${
          process.env.REACT_APP_AXIOS_API_POST
        }`,
        { longUrl: this.state.value }
      );
      this.setState({ shortUrl: res.shortUrl });
    }
  };

  // Handles the click of individual urls, and then returns the updated list
  // This functionality isn't working 100%...
  handleItemClick() {
    this.getUrlList();
  }

  // Get full list of all urls persisted in the database.
  getUrlList = async () => {
    const { data: items } = await axios.get(
      `http://localhost:${process.env.REACT_APP_AXIOS_PORT}/${
        process.env.REACT_APP_AXIOS_API_GETALL
      }`
    );
    this.setState({ items });
  };

  // on mount, set a timer that calls the api to get full list of urls every 5 seconds... "polling" feature :)
  componentDidMount() {
    this.getUrlList();
    this.timer = setInterval(() => this.getUrlList(), 5000);
  }

  // clear and kill the timer on unmount.
  componentWillUnmount() {
    clearInterval(this.timer);
    this.timer = null;
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotron">
          <h1 className="display-3 text-center">Short URL Generator</h1>
          <p className="lead">
            This is a simple app that generates a short url. Please follow
            instructions below.
          </p>
          <hr className="my-4" />
          <form
            className="needs-validation"
            noValidate
            onSubmit={this.handleSubmit}
          >
            <div className="form-row">
              <div className="col-md-8">
                <input
                  type="url"
                  className="form-control"
                  id="longUrlInput"
                  placeholder="Please type long URL here"
                  value={this.state.value}
                  onChange={this.handleChange}
                  required
                />
                <div className="invalid-feedback">Please enter a valid URL</div>
              </div>
              <div className="col-md-4">
                <button className="btn btn-primary" type="submit">
                  Submit form
                </button>
              </div>
            </div>
          </form>
          {this.state.shortUrl && (
            <div className="row">
              <div className="col-md">
                New Short URL:{" "}
                <a
                  href={this.state.shortUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                    onClick={this.handleItemClick}
                >
                  {this.state.shortUrl}
                </a>
              </div>
            </div>
          )}
        </div>
        <div className="row">
          <div className="col text-center">
            <h3 className="display-5">
              Short URL Stats <small>(polling every 5 seconds)</small>
            </h3>
          </div>
        </div>
        {/* Using React Context here just to see how it works... */}
        <ShortUrlProvider
          value={{ items: this.state.items, click: this.handleItemClick }}
        >
          <Items />
        </ShortUrlProvider>
      </div>
    );
  }
}

export default App;
