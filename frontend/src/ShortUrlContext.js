import React from "react";

const ShortUrlContext = React.createContext({});

export const ShortUrlProvider = ShortUrlContext.Provider;
export const ShortUrlConsumer = ShortUrlContext.Consumer;
