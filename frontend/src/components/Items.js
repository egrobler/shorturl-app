import React, { Component } from "react";
import { ShortUrlConsumer } from "../ShortUrlContext";

class Items extends Component {
  render() {
    /* Using React Context here just to see how it works... */

    return (
      <ShortUrlConsumer>
        {context => (
          <div className="row">
            <div className="col">
              <table className="table table-sm table-responsive">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Short URL</th>
                    <th scope="col">Visited?</th>
                    <th scope="col">Nr of clicks</th>
                    <th scope="col">Long URL</th>
                  </tr>
                </thead>
                <tbody>
                  {context.items.map((item, i) => {
                    return (
                      <tr key={item._id}>
                        <th scope="row">{i}</th>
                        <td>
                          <a
                            href={item.shortUrl}
                            target="_blank"
                            rel="noopener noreferrer"
                            onClick={context.click}
                          >
                            {item.shortUrl}
                          </a>
                        </td>
                        <td>{item.visited ? "Yes" : "No"}</td>
                        <td>{item.nrClicks}</td>
                        <td>{item.longUrl}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        )}
      </ShortUrlConsumer>
    );
  }
}

export default Items;
